from setuptools import setup, find_packages

setup(
    name="pylenspice",
    version=0.1,
    packages=find_packages(),
)